package ru.t1.mayornikov.tm.command.system;

import ru.t1.mayornikov.tm.api.service.IProjectService;
import ru.t1.mayornikov.tm.api.service.ITaskService;
import ru.t1.mayornikov.tm.api.service.IUserService;
import ru.t1.mayornikov.tm.enumerated.Role;
import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.model.Project;
import ru.t1.mayornikov.tm.model.Task;

public class DemoDataInitCommand extends AbstractSystemCommand {

    protected IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    protected IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    private final static String NAME = "demo-data";

    private final static String DESCRIPTION = "Initialize demo data.";

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        getUserService().create("test", "test", "test@test.com");
        getUserService().create("user", "user", "abvgd_mail@mail.ru");
        getUserService().create("admin", "admin", Role.ADMIN);

        getProjectService().add(new Project("SUPER PROJECT one", "", Status.IN_PROGRESS));
        getProjectService().add(new Project("DUPER PROJECT two", "Like super project one but duper.", Status.NOT_STARTED));
        getProjectService().add(new Project("donnuiy", "This project status is COMPLETED.", Status.COMPLETED));

        getTaskService().add(new Task("delo", "delat", Status.IN_PROGRESS));
        getTaskService().add(new Task("BIG DEAL", "GREAT LIFE", Status.NOT_STARTED));
        getTaskService().add(new Task("order", "make model Oleg", Status.COMPLETED));
        System.out.println("[DEMO LOAD SUCCESS]");
    }

}