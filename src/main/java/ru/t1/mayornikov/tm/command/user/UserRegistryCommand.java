package ru.t1.mayornikov.tm.command.user;

import ru.t1.mayornikov.tm.api.service.IAuthService;
import ru.t1.mayornikov.tm.model.User;
import ru.t1.mayornikov.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractUserCommand {

    private final static String NAME = "user-registry";

    private final static String DESCRIPTION = "Registry new user.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        final IAuthService authService = getAuthService();
        final User user = authService.registry(login, password, email);
        showUser(user);
    }

}
