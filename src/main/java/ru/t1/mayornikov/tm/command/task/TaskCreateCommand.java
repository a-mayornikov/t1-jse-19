package ru.t1.mayornikov.tm.command.task;

import ru.t1.mayornikov.tm.util.TerminalUtil;

public class TaskCreateCommand extends AbstractTaskCommand{

    private final static String NAME = "task-create";

    private final static String DESCRIPTION = "Create task.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TO DO:");
        final String description = TerminalUtil.nextLine();
        getTaskService().create(name, description);
    }

}