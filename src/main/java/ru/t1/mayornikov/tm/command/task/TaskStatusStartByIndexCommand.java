package ru.t1.mayornikov.tm.command.task;

import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.util.TerminalUtil;

public class TaskStatusStartByIndexCommand extends AbstractTaskCommand{

    private final static String NAME = "task-start-by-index";

    private final static String DESCRIPTION = "Set task status to started by index.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK]");
        System.out.println("ENTER TASK INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().changeTaskStatus(index, Status.IN_PROGRESS);
    }

}