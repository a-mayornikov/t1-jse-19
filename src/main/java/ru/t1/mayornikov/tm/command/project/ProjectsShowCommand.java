package ru.t1.mayornikov.tm.command.project;

public final class ProjectsShowCommand extends AbstractProjectCommand {

    private static final String NAME = "project-list";

    private static final String DESCRIPTION = "Show all projects.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECTS]");
        getProjectService().renderProjects();
    }

}
