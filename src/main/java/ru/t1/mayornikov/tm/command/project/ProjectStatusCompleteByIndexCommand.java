package ru.t1.mayornikov.tm.command.project;

import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.util.TerminalUtil;

public class ProjectStatusCompleteByIndexCommand extends AbstractProjectCommand{

    private static final String NAME = "project-complete-by-index";

    private static final String DESCRIPTION = "Set status project to completed by index.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT]");
        System.out.println("ENTER PROJECT INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectService().changeProjectStatus(index, Status.COMPLETED);
    }

}