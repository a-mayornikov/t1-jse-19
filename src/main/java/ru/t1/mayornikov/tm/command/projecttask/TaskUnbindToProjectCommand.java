package ru.t1.mayornikov.tm.command.projecttask;

import ru.t1.mayornikov.tm.util.TerminalUtil;

public class TaskUnbindToProjectCommand extends AbstractProjectTaskCommand{

    private final static String NAME = "task-unbind-to-project";

    private final static String DESCRIPTION = "Unbind task to project.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();
        getProjectTaskService().unbindTaskFromProject(projectId, taskId);
    }

}