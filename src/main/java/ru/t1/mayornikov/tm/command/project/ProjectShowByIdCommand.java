package ru.t1.mayornikov.tm.command.project;

import ru.t1.mayornikov.tm.model.Project;
import ru.t1.mayornikov.tm.util.TerminalUtil;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    private static final String NAME = "project-show-by-id";

    private static final String DESCRIPTION = "Show project by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final Project project = getProjectService().findOne(TerminalUtil.nextLine());
        getProjectService().showProject(project);
    }

}