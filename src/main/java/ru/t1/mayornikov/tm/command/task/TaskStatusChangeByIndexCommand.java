package ru.t1.mayornikov.tm.command.task;

import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.util.TerminalUtil;

import java.util.Arrays;

public class TaskStatusChangeByIndexCommand extends AbstractTaskCommand{

    private final static String NAME = "task-status-change-by-index";

    private final static String DESCRIPTION = "Set task status by index.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER TASK INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        final Status status = Status.toStatus(TerminalUtil.nextLine());
        getTaskService().changeTaskStatus(index, status);
    }

}