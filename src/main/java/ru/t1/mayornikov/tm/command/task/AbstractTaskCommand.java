package ru.t1.mayornikov.tm.command.task;

import ru.t1.mayornikov.tm.api.service.ITaskService;
import ru.t1.mayornikov.tm.command.AbstractCommand;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

}