package ru.t1.mayornikov.tm.command.project;

public class ProjectsClearCommand extends AbstractProjectCommand{

    private static final String NAME = "projects-remove";

    private static final String DESCRIPTION = "Remove all projects.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECTS]");
        getProjectService().clear();
    }

}