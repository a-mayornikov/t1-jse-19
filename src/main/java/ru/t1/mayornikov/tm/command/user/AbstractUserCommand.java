package ru.t1.mayornikov.tm.command.user;

import ru.t1.mayornikov.tm.api.service.IAuthService;
import ru.t1.mayornikov.tm.api.service.IUserService;
import ru.t1.mayornikov.tm.command.AbstractCommand;
import ru.t1.mayornikov.tm.exception.entity.UserNotFoundException;
import ru.t1.mayornikov.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    public IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    public IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    protected void showUser(final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    public String getArgument() {
        return null;
    }

}