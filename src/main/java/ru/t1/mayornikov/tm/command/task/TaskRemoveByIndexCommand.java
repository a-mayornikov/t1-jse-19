package ru.t1.mayornikov.tm.command.task;

import ru.t1.mayornikov.tm.util.TerminalUtil;

public class TaskRemoveByIndexCommand extends AbstractTaskCommand{

    private final static String NAME = "task-remove-by-index";

    private final static String DESCRIPTION = "Remove task by index.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        getTaskService().remove(index);
    }

}