package ru.t1.mayornikov.tm.command;

import ru.t1.mayornikov.tm.api.model.ICommand;
import ru.t1.mayornikov.tm.api.service.IServiceLocator;

public abstract class AbstractCommand implements ICommand {

    private final static String SEPARATOR = ": ";

    private final static String EMPTY_NAME = "empty_name: ";

    private final static String EMPTY_OBJECT = "No information...";

    protected IServiceLocator serviceLocator;

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name + SEPARATOR;
        else result += EMPTY_NAME;
        if (argument != null && !argument.isEmpty()) result += argument + SEPARATOR;
        if (description != null && !description.isEmpty()) result += description;
        if (result.equals(EMPTY_NAME)) result = EMPTY_OBJECT;
        return result;
    }

}