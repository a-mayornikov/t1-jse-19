package ru.t1.mayornikov.tm.command.task;

import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.util.TerminalUtil;

public class TaskStatusCompleteByIdCommand extends AbstractTaskCommand{

    private final static String NAME = "task-complete-by-id";

    private final static String DESCRIPTION = "Set task status to completed by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK]");
        System.out.println("ENTER TASK ID:");
        final String id = TerminalUtil.nextLine();
        getTaskService().changeTaskStatus(id, Status.IN_PROGRESS);
    }

}