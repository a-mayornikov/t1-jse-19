package ru.t1.mayornikov.tm.command.task;

import ru.t1.mayornikov.tm.model.Task;
import ru.t1.mayornikov.tm.util.TerminalUtil;

public class TaskUpdateByIdCommand extends AbstractTaskCommand{

    private final static String NAME = "task-update-by-id";

    private final static String DESCRIPTION = "Update task by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY ID]");
        System.out.println("ENTER ID:");
        final Task task = getTaskService().findOne(TerminalUtil.nextLine());
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TO DO:");
        final String description = TerminalUtil.nextLine();
        task.setName(name);
        task.setDescription(description);
    }

}