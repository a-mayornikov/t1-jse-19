package ru.t1.mayornikov.tm.command.task;

import ru.t1.mayornikov.tm.util.TerminalUtil;

public class TaskRemoveByIdCommand extends AbstractTaskCommand{

    private final static String NAME = "task-remove-by-id";

    private final static String DESCRIPTION = "Remove task by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getTaskService().remove(id);
    }

}