package ru.t1.mayornikov.tm.command.user;

public final class UserLogoutCommand extends AbstractUserCommand{

    private final static String NAME = "user-logout";

    private final static String DESCRIPTION = "Logout from active user.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
    }

}
