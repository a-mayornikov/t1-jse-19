package ru.t1.mayornikov.tm.command.system;

import ru.t1.mayornikov.tm.api.model.ICommand;
import ru.t1.mayornikov.tm.command.AbstractCommand;

import java.util.Collection;

public class ApplicationHelpShowCommand extends AbstractSystemCommand{

    private final static String NAME = "help";

    private final static String DESCRIPTION = "Show commands.";

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) System.out.println(command);
    }

}