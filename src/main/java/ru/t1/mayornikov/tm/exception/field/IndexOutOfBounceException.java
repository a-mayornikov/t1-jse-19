package ru.t1.mayornikov.tm.exception.field;

public final class IndexOutOfBounceException extends AbstractFieldException{

    public IndexOutOfBounceException() {
        super("Index is out of bounce...");
    }

}