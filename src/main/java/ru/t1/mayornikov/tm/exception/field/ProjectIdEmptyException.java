package ru.t1.mayornikov.tm.exception.field;

public final class ProjectIdEmptyException extends AbstractFieldException{

    public ProjectIdEmptyException() {
        super("Project id is empty...");
    }
    
}