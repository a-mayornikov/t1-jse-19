package ru.t1.mayornikov.tm.exception.entity;

public final class ExistsEmailException extends AbstractEntityException{

    public ExistsEmailException() {
        super("No one User use this email...");
    }

}