package ru.t1.mayornikov.tm.exception.field;

public final class IdEmptyException extends AbstractFieldException{

    public IdEmptyException() {
        super("Id is empty...");
    }
    
}