package ru.t1.mayornikov.tm.exception.system;

public final class AccessDeniedException extends AbstractSystemException{

    public AccessDeniedException() {
        super("Access denied...");
    }

}