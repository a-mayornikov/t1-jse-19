package ru.t1.mayornikov.tm.exception.field;

public final class LoginEmptyException extends AbstractFieldException{

    public LoginEmptyException() {
        super("Login is empty...");
    }
    
}