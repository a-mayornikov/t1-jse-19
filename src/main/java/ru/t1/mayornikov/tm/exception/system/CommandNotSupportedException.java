package ru.t1.mayornikov.tm.exception.system;

public final class CommandNotSupportedException extends AbstractSystemException{

    public CommandNotSupportedException() {
        super("Command not supported...");
    }

    public CommandNotSupportedException(final String command) {
        super("Command \"" + command + "\" is not supported...\nUse 'help' for more information.");
    }

}