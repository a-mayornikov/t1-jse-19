package ru.t1.mayornikov.tm.exception.field;

public final class RoleEmptyException extends AbstractFieldException{

    public RoleEmptyException() {
        super("Role is empty...");
    }
    
}