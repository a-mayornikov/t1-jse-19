package ru.t1.mayornikov.tm.constant;

import java.text.DecimalFormat;

public final class FormatConst {

    public static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.##");

    public static final double BYTES_K = 1024;

    public static final double BYTES_M = BYTES_K * 1024;

    public static final double BYTES_G = BYTES_M * 1024;

    public static final double BYTES_T = BYTES_G * 1024;

    public static final double BYTES_P = BYTES_T * 1024;

    public static final String BYTES_NAME = "B";

    public static final String BYTES_K_NAME = "KB";

    public static final String BYTES_M_NAME = "MB";

    public static final String BYTES_G_NAME = "GB";

    public static final String BYTES_T_NAME = "TB";

    public static final String BYTES_P_NAME = "PB";

    public static final String SEPARATOR = " ";

    private FormatConst(){
    }

}