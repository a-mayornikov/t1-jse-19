package ru.t1.mayornikov.tm.component;

import ru.t1.mayornikov.tm.api.repository.ICommandRepository;
import ru.t1.mayornikov.tm.api.repository.IProjectRepository;
import ru.t1.mayornikov.tm.api.repository.ITaskRepository;
import ru.t1.mayornikov.tm.api.repository.IUserRepository;
import ru.t1.mayornikov.tm.api.service.*;
import ru.t1.mayornikov.tm.command.AbstractCommand;
import ru.t1.mayornikov.tm.command.system.DemoDataInitCommand;
import ru.t1.mayornikov.tm.command.project.*;
import ru.t1.mayornikov.tm.command.projecttask.TaskBindToProjectCommand;
import ru.t1.mayornikov.tm.command.projecttask.TaskUnbindToProjectCommand;
import ru.t1.mayornikov.tm.command.system.ApplicationExitCommand;
import ru.t1.mayornikov.tm.command.system.ApplicationHelpShowCommand;
import ru.t1.mayornikov.tm.command.system.ApplicationInformationShowCommand;
import ru.t1.mayornikov.tm.command.system.SystemInformationShowCommand;
import ru.t1.mayornikov.tm.command.task.*;
import ru.t1.mayornikov.tm.command.user.*;
import ru.t1.mayornikov.tm.exception.system.CommandNotSupportedException;
import ru.t1.mayornikov.tm.repository.CommandRepository;
import ru.t1.mayornikov.tm.repository.ProjectRepository;
import ru.t1.mayornikov.tm.repository.TaskRepository;
import ru.t1.mayornikov.tm.repository.UserRepository;
import ru.t1.mayornikov.tm.service.*;
import ru.t1.mayornikov.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator{

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        registry(new DemoDataInitCommand());
        registry(new ApplicationHelpShowCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationInformationShowCommand());
        registry(new SystemInformationShowCommand());

        registry(new ProjectCreateCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectsClearCommand());
        registry(new ProjectStatusStartByIdCommand());
        registry(new ProjectStatusStartByIndexCommand());
        registry(new ProjectStatusCompleteByIdCommand());
        registry(new ProjectStatusCompleteByIndexCommand());
        registry(new ProjectStatusChangeByIdCommand());
        registry(new ProjectStatusChangeByIndexCommand());
        registry(new ProjectsShowCommand());
        registry(new ProjectsShowSortedCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());

        registry(new TaskCreateCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new TaskStatusStartByIdCommand());
        registry(new TaskStatusStartByIndexCommand());
        registry(new TaskStatusCompleteByIdCommand());
        registry(new TaskStatusCompleteByIndexCommand());
        registry(new TaskStatusChangeByIdCommand());
        registry(new TaskStatusChangeByIndexCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TasksRemoveCommand());
        registry(new TasksShowCommand());
        registry(new TasksShowSortedCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskShowByProjectIdCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindToProjectCommand());

        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void processCommands() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                Thread.sleep(50);
                System.out.println();
                System.out.println("ENTER COMMAND: ");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[DONE]");
                loggerService.command(command);
            } catch (final Exception e) {
                System.out.println("[FAIL]");
                loggerService.error(e);
            }
        }
    }

    private void processArguments(final String[] arguments) {
        if (arguments == null || arguments.length < 1) return;
        processArgument(arguments[0]);
        exit();
    }

    private void exit() {
        System.exit(0);
    }

    private void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        abstractCommand.execute();
    }

    private void processArgument(final String argument) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new CommandNotSupportedException(argument);
        abstractCommand.execute();
    }

    private void initLogger() {
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> loggerService.info("*** TASK MANAGER IS SHUTTING DOWN ***")));
    }

    public void run(final String... args){
        processArguments(args);
        initLogger();
        processCommands();
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}