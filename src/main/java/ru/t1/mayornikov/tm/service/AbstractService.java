package ru.t1.mayornikov.tm.service;

import ru.t1.mayornikov.tm.api.repository.IRepository;
import ru.t1.mayornikov.tm.api.service.IService;
import ru.t1.mayornikov.tm.enumerated.Sort;
import ru.t1.mayornikov.tm.exception.field.IdEmptyException;
import ru.t1.mayornikov.tm.exception.field.IndexEmptyException;
import ru.t1.mayornikov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    protected final R repository;

    public AbstractService(final R repository) {
        this.repository = repository;
    }

    @Override
    public M add(final M model) {
        if (model == null) return null;
        return repository.add(model);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        if (comparator == null) findAll();
        return repository.findAll(comparator);
    }

    @Override
    public List<M> findAll(final Sort sort) {
        if (sort == null) findAll();
        return repository.findAll((Comparator<M>) sort.getComparator());
    }

    @Override
    public M findOne(final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        return repository.findOne(id);
    }

    @Override
    public M findOne(final Integer index) {
        if (index == null) throw new IndexEmptyException();
        return repository.findOne(index);
    }

    @Override
    public boolean existsById(final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @Override
    public M remove(final M model) {
        if (model == null) return null;
        return repository.remove(model);
    }

    @Override
    public M remove(final String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        return repository.remove(id);
    }

    @Override
    public M remove(final Integer index) {
        if (index == null) throw new IndexEmptyException();
        return repository.remove(index);
    }

    @Override
    public Integer getSize() {
        return repository.getSize();
    }

}