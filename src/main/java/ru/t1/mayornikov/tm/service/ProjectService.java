package ru.t1.mayornikov.tm.service;

import ru.t1.mayornikov.tm.api.repository.IProjectRepository;
import ru.t1.mayornikov.tm.api.service.IProjectService;
import ru.t1.mayornikov.tm.enumerated.Status;
import ru.t1.mayornikov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.mayornikov.tm.exception.field.*;
import ru.t1.mayornikov.tm.model.Project;

import java.util.List;

public class ProjectService extends AbstractService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(final IProjectRepository repository) {
        super(repository);
    }

    @Override
    public void showProject(final Project project) {
        if (project == null) return;
        final String id = project.getId();
        final String name = project.getName();
        final String description = project.getDescription();
        final Status status = project.getStatus();
        if (!"".equals(id)) System.out.println("ID: " + id);
        if (!"".equals(name)) System.out.println("NAME: " + name);
        if (!"".equals(description)) System.out.println("DESCRIPTION: " + description);
        if (status != null) System.out.println("STATUS: " + Status.toName(status));
    }

    @Override
    public void renderProjects() {
        int index = 1;
        for (final Project project : findAll()) {
            if (project == null) continue;
            System.out.println(index++ + ".");
            showProject(project);
        }
        if (index == 1) System.out.println("No one project found...");
    }

    @Override
    public void renderProjects(final List<Project> projects) {
        int index = 1;
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index++ + ".");
            showProject(project);
        }
        if (index == 1) System.out.println("No one project found...");
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(name, description);
    }

    @Override
    public Project create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(name);
    }

    @Override
    public Project update(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOne(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project update(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOne(index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project changeProjectStatus(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Project project = findOne(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatus(final Integer index, final Status status) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (index >= repository.getSize()) throw new IndexOutOfBounceException();
        final Project project = findOne(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

}