package ru.t1.mayornikov.tm.api.service;

import ru.t1.mayornikov.tm.api.repository.IRepository;
import ru.t1.mayornikov.tm.enumerated.Sort;
import ru.t1.mayornikov.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    List<M> findAll(Sort sort);

}