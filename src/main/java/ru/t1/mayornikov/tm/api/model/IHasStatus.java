package ru.t1.mayornikov.tm.api.model;

import ru.t1.mayornikov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
